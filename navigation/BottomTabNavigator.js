import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import * as React from 'react';
import {connect} from 'react-redux';
import {OswaldText} from "../components/StyledText";
import {FilterButton} from "../components/Buttons";

import {Icon, Label} from '../components/TabBar';
import ShopScreen from '../screens/ShopScreen';
import CartScreen from "../screens/CartScreen";
import FavouritesScreen from "../screens/FavouritesScreen";
import ProfileScreen from "../screens/ProfileScreen";
import SignInScreen from "../screens/SignInScreen";

const BottomTab = createBottomTabNavigator();
const INITIAL_ROUTE_NAME = 'Магазин';

const BottomTabNavigator = ({navigation, route, auth}) => {
  // Set the header title on the parent stack navigator depending on the
  // currently active tab. Learn more in the documentation:
  // https://reactnavigation.org/docs/en/screen-options-resolution.html
  navigation.setOptions(getHeaderTitle(route, navigation));

  return (
    <BottomTab.Navigator initialRouteName={INITIAL_ROUTE_NAME}>
      <BottomTab.Screen
        name="Shop"
        component={ShopScreen}
        options={{
          tabBarIcon: ({focused}) => <Icon focused={focused} name="ios-home"/>,
          tabBarLabel: ({focused}) => <Label focused={focused} name="Магазин"/>,
        }}
      />
      <BottomTab.Screen
        name="Cart"
        component={CartScreen}
        options={{
          tabBarIcon: ({focused}) => <Icon focused={focused} name="ios-cart"/>,
          tabBarLabel: ({focused}) => <Label focused={focused} name="Корзина"/>,
        }}
      />
      <BottomTab.Screen
        name="Favourites"
        component={FavouritesScreen}
        options={{
          tabBarIcon: ({focused}) => <Icon focused={focused} name="ios-heart"/>,
          tabBarLabel: ({focused}) => <Label focused={focused} name="Избранное"/>,
        }}
      />
      <BottomTab.Screen
        name="Profile"
        component={auth ? ProfileScreen : SignInScreen}
        options={{
          tabBarIcon: ({focused}) => <Icon focused={focused} name="ios-contact"/>,
          tabBarLabel: ({focused}) => <Label focused={focused} name="Профиль"/>,
        }}
      />
    </BottomTab.Navigator>
  );
}

export default connect(
  state => ({
    auth: state.profile.authorized
  })
)(BottomTabNavigator);

function getHeaderTitle(route, navigation) {
  const routeName = route.state?.routes[route.state.index]?.name ?? INITIAL_ROUTE_NAME;

  const getTitle = name => <OswaldText style={{textTransform: 'uppercase'}}>{name}</OswaldText>;

  switch (routeName) {
    case 'Shop':
      return {
        headerTitle: getTitle('Магазин'),
        headerRight: () => <FilterButton onPress={() => navigation.navigate('Filters')}/>
      };
    case 'Cart':
      return {
        headerTitle: getTitle('Корзина'),
        headerRight: null
      };
    case 'Favourites':
      return {
        headerTitle: getTitle('Избранное'),
        headerRight: null
      }
    case 'Profile':
      return {
        headerTitle: getTitle('Профиль'),
        headerRight: null
      };
  }
}
