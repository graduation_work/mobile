import * as Linking from 'expo-linking';

export default {
  prefixes: [Linking.makeUrl('/')],
  config: {
    Root: {
      path: 'root',
      screens: {
        Shop: 'shop',
        Cart: 'cart',
        Favourites: 'favourites',
        Profile: 'profile',
        SignIn: 'signin',
        SignUp: 'signup',
        Product: 'product',
        Filters: 'filters',
        Order: 'order',
      },
    },
  },
};
