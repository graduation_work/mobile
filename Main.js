import React, {useEffect} from "react";
import {Platform, StatusBar, View} from "react-native";
import {NavigationContainer} from "@react-navigation/native";
import createStackNavigator from "@react-navigation/stack/src/navigators/createStackNavigator";
import {connect} from 'react-redux';
import * as api from "./api";
import {deleteData, getData} from "./storage";
import {CART_KEY, FAVOURITES_KEY} from "./helpers";
import * as Updates from 'expo-updates';
import {FilterButton} from "./components/Buttons";
import {OswaldText} from "./components/StyledText";

import {setCart, setFavourites, signIn} from "./redux/actions";

import LinkingConfiguration from "./navigation/LinkingConfiguration";
import BottomTabNavigator from "./navigation/BottomTabNavigator";
import SignInScreen from "./screens/SignInScreen";
import SignUpScreen from "./screens/SignUpScreen";
import ProductScreen from "./screens/ProductScreen";
import FilterScreen from "./screens/FilterScreen";
import OrderScreen from "./screens/OrderScreen";

const Stack = createStackNavigator();

const Main = props => {
  useEffect(() => {
    getData('token').then(result => {
      if (!result) {
        getData(CART_KEY).then(result => !result ? props.setCart([]) : props.setCart(result));
        getData(FAVOURITES_KEY).then(result => !result ? props.setFavourites([]) : props.setFavourites(result));
      } else {
        api.setToken(result);
        api.getProfile()
          .then(response => {
            props.getProfile(response.data.user);
            props.setCart(response.data.cart);
            props.setFavourites(response.data.favourites);
          })
          .catch(() => {
            deleteData('token').then(() => Updates.reloadAsync());
          });
      }
    });
  }, []);

  return (
    <View style={styles.container}>
      {Platform.OS === 'ios' && <StatusBar barStyle="dark-content"/>}
      <NavigationContainer linking={LinkingConfiguration}>
        <Stack.Navigator>
          <Stack.Screen
            name="Shop"
            component={BottomTabNavigator}
            options={props => ({
              headerTitle: <OswaldText style={styles.headerText}>Магазин</OswaldText>,
              headerRight: () => <FilterButton onPress={() => props.navigation.navigate('Filters')}/>
            })}
          />
          <Stack.Screen
            name="SignIn"
            component={SignInScreen}
            options={{
              headerTitle: <OswaldText style={styles.headerText}>Вход</OswaldText>
            }}
          />
          <Stack.Screen
            name="SignUp"
            component={SignUpScreen}
            options={{
              headerTitle: <OswaldText style={styles.headerText}>Регистрация</OswaldText>
            }}
          />
          <Stack.Screen
            name="Product"
            component={ProductScreen}
            options={item => ({
              headerTitle: <OswaldText style={styles.headerText}>{item.route.params.title}</OswaldText>,
            })}
          />
          <Stack.Screen
            name="Filters"
            component={FilterScreen}
            options={{
              headerTitle: <OswaldText style={styles.headerText}>Фильтры</OswaldText>,
            }}
          />
          <Stack.Screen
            name="Order"
            component={OrderScreen}
            options={item => ({
              headerTitle: <OswaldText style={styles.headerText}>Заказ #{item.route.params.id}</OswaldText>,
            })}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </View>
  );
};

export default connect(
  state => ({
    profile: state.profile,
    cart: state.cart,
    favourites: state.favourites,
  }),
  dispatch => ({
    getProfile: user => dispatch(signIn(user)),
    setCart: data => dispatch(setCart(data)),
    setFavourites: data => dispatch(setFavourites(data)),
  })
)(Main);

const styles = {
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  headerText: {
    textTransform: 'uppercase',
  }
};