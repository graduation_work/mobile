import {Ionicons} from '@expo/vector-icons';
import * as React from 'react';
import {OswaldText} from "./StyledText";

import Colors from '../constants/Colors';

export const Icon = props => <Ionicons
  name={props.name}
  size={30}
  style={{marginBottom: -3}}
  color={props.focused ? Colors.tabIconSelected : Colors.tabIconDefault}
/>;

export const Label = props => {
  const color = props.focused ? Colors.tabIconSelected : Colors.tabIconDefault;
  return (<OswaldText style={{color: color, textTransform: 'uppercase'}}>{props.name}</OswaldText>
  );
}
