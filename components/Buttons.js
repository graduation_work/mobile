import React from 'react';
import {TouchableOpacity} from "react-native";
import {OswaldText} from "./StyledText";
import {Ionicons} from "@expo/vector-icons";

export const ShopButton = props => {
  const content = (() => {
    if (props.title)
      return <OswaldText style={{
        color: props.color === 'white' ? 'black' : 'white',
        textTransform: 'uppercase',
        fontSize: 18
      }}>
        {props.title}
      </OswaldText>;

    if (props.icon)
      return <Ionicons
        name={props.icon}
        size={20}
        color={props.color === 'white' ? 'black' : 'white'}
      />
  })()

  return (
    <TouchableOpacity
      style={[props.style, {
        backgroundColor: props.color === 'white' ? 'white' : 'black',
        alignItems: 'center',
        borderColor: props.color === 'white' ? 'black' : null,
        borderWidth: props.color === 'white' ? 0.5 : null,
        padding: 10
      }]}
      onPress={props.onPress}
    >
      {content}
    </TouchableOpacity>
  );
};

export const FilterButton = props => {
  return (<TouchableOpacity style={[props.style, {margin: 20}]} onPress={props.onPress}>
    <Ionicons name='ios-funnel' size={20} />
  </TouchableOpacity>);
};