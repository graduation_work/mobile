import React from 'react';
import {ScrollView, View, SafeAreaView} from "react-native";
import {OswaldText} from "./StyledText";
import {Button, Input} from "react-native-elements";
import {Ionicons} from "@expo/vector-icons";
import {SignStyles} from "./StyleSheets";

export default function SignIn(
  {
    isEntering,
    onChangeEmail,
    onChangePassword,
    invalidCredentials,
    handleSignInClick,
    navigation,
  }
) {
  return (
    <SafeAreaView>
      <ScrollView>
        <View style={SignStyles.container}>
          <OswaldText style={SignStyles.header}>Вход</OswaldText>
          <View style={SignStyles.body}>
            <Input
              leftIcon={<Ionicons
                name='ios-mail'
                size={24}
                color='#ccc'
              />}
              label='Электронная почта'
              labelStyle={SignStyles.inputLabel}
              onChangeText={value => onChangeEmail(value)}
            />
            <Input
              leftIcon={<Ionicons
                name='ios-lock'
                size={24}
                color='#ccc'
              />}
              label='Пароль'
              labelStyle={SignStyles.inputLabel}
              secureTextEntry={true}
              onChangeText={value => onChangePassword(value)}
              errorMessage={invalidCredentials ? 'Неправильный логин или пароль' : null}
            />
            <Button
              type='solid'
              title={isEntering ? 'Пожалуйста, подождите' : 'Войти'}
              buttonStyle={SignStyles.buttonMain}
              titleStyle={SignStyles.buttonMainTitle}
              onPress={handleSignInClick}
            />
            <Button
              type='outline'
              title='Нет аккаунта? Регистрация'
              buttonStyle={SignStyles.buttonSecondary}
              titleStyle={SignStyles.buttonSecondaryTitle}
              onPress={() => navigation.navigate('SignUp')}
            />
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}