import React from 'react';
import {FlatList, SafeAreaView} from "react-native";
import ProductCard from "./ProductCard";

export default function Cards({products, loadProducts, cart, favourites, navigation}) {
  return (
    <SafeAreaView>
      <FlatList
        onEndReached={loadProducts}
        data={products}
        renderItem={({item}) => <ProductCard
          key={item.id}
          navigation={navigation}
          product={item}
          favourites={favourites}
          cart={cart}
        />}
        numColumns={2}
      />
    </SafeAreaView>
  );
}