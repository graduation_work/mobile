import React from 'react';
import {Image, SafeAreaView, ScrollView, StyleSheet, View, Dimensions} from "react-native";
import Carousel, {Pagination} from "react-native-snap-carousel";
import {OswaldText} from "./StyledText";
import {ShopButton} from "./Buttons";

const width = Dimensions.get('window').width;

export default function Product(
  {
    product,
    previewIndex, setPreviewIndex,
    handleToCartClick,
    handleToFavouritesClick,
    cart, favourite,
  }
) {
  const imageElement = ({item, index}) => {
    return <Image
      source={{uri: item}}
      style={styles.image}
      resizeMode='contain'
    />;
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <ScrollView>
        <Carousel
          layout='default'
          itemWidth={width}
          data={product.images}
          renderItem={imageElement}
          sliderWidth={width}
          containerCustomStyle={{backgroundColor: 'white'}}
          onSnapToItem={index => setPreviewIndex(index)}
        />
        <Pagination
          dotsLength={product.images.length}
          activeDotIndex={previewIndex}
          dotStyle={{backgroundColor: 'black'}}
          containerStyle={{margin: -15}}
          inactiveDotOpacity={0.6}
          inactiveDotScale={0.6}
        />
        <View style={styles.body}>
          <OswaldText style={styles.headingText}>{product.name}</OswaldText>
          <OswaldText style={styles.paragraphText}>{product.description}</OswaldText>
          <OswaldText style={styles.priceText}>{product.price} Р</OswaldText>
          <ShopButton
            title={cart ? 'Убрать из корзины' : 'В корзину'}
            color={cart ? 'black' : 'white'}
            style={styles.button}
            onPress={() => handleToCartClick(product)}
          />
          <ShopButton
            title={favourite ? 'Убрать из избранного' : 'В избранное'}
            color={favourite ? 'black' : 'white'}
            style={styles.button}
            onPress={() => handleToFavouritesClick(product)}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  body: {
    padding: 10,
  },
  headingText: {
    fontSize: 36,
    textTransform: 'uppercase',
  },
  paragraphText: {
    fontSize: 20,
  },
  priceText: {
    fontSize: 36,
  },
  image: {
    margin: 0,
    padding: 0,
    width: '100%',
    height: width * 0.7
  },
  button: {
    marginTop: 10,
  }
});