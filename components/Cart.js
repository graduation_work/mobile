import React from 'react';
import {ScrollView, TextInput, View, TouchableOpacity} from "react-native";
import {Button, Divider, ListItem} from "react-native-elements";
import {OswaldText} from "./StyledText";
import {Ionicons} from "@expo/vector-icons";
import {ShopButton} from "./Buttons";

export default function Cart(
  {
    auth,
    products,
    totalPrice,
    emailError,
    orderInProcess,
    handleCountChange,
    handleBuyClick,
    onChangeEmail,
    handleItemClick
  }
) {

  const emailInput = () => (
    <View>
      <View style={styles.row}>
        <View style={styles.emailTextWrapper}>
          <OswaldText style={styles.text}>Email</OswaldText>
        </View>
        <View style={styles.emailInputWrapper}>
          <TextInput
            onChange={(e) => onChangeEmail(e.nativeEvent.text.trim())}
            style={[{borderColor: emailError ? 'red' : 'black'}, styles.emailInput]}
          />
        </View>
      </View>
      <OswaldText style={styles.emailHelpText}>Будет использоваться для связи. Сюда будут присланы детали
        заказа.</OswaldText>
    </View>
  );

  return (
    <ScrollView>
      <View style={styles.container}>
        <View>
          {
            products.map(item => (
              <TouchableOpacity
                key={item.id}
                onPress={() => handleItemClick(item)}
              >
                <ListItem
                  leftAvatar={{source: {uri: item.image}}}
                  title={<OswaldText style={{textTransform: 'uppercase'}}>{item.name}</OswaldText>}
                  subtitle={<OswaldText>{item.price} Р</OswaldText>}
                  rightSubtitle={
                    <View style={styles.countBlock}>
                      <TextInput
                        style={styles.countInput}
                        defaultValue={item.count.toString()}
                        keyboardType='numeric'
                        onEndEditing={(e) => handleCountChange(item, e.nativeEvent.text)}
                      />
                      <OswaldText style>шт.</OswaldText>
                    </View>
                  }
                  rightIcon={<Button
                    type='clear'
                    titleStyle={styles.buttonTitle}
                    buttonStyle={styles.button}
                    icon={<Ionicons name='ios-close-circle-outline' color='black' size={25}/>}
                    onPress={() => handleCountChange(item, 0)}
                  />}
                />
              </TouchableOpacity>
            ))
          }
        </View>
        <View style={styles.footer}>
          <Divider/>
          <View style={styles.row}>
            <View>
              <OswaldText style={styles.text}>Итого</OswaldText>
            </View>
            <View>
              <OswaldText style={styles.text}>{totalPrice} Р</OswaldText>
            </View>
          </View>
          {auth ? null : emailInput()}
          <View>
            <ShopButton
              title={orderInProcess ? 'Пожалуйста, подождите...' : 'Оформить заказ'}
              style={styles.buyButton}
              onPress={handleBuyClick}
            />
          </View>
        </View>
      </View>
    </ScrollView>
  );
}


const styles = {
  container: {},
  countBlock: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  countInput: {
    textAlign: 'center',
    fontFamily: 'Oswald'
  },
  buttonTitle: {
    color: 'black'
  },
  button: {
    borderColor: 'black',
    borderRadius: 0.1
  },
  footer: {
    padding: 10,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  text: {
    fontSize: 30,
    textTransform: 'uppercase',
  },
  buyButton: {
    marginTop: 10,
  },
  emailTextWrapper: {
    width: '30%',
  },
  emailInputWrapper: {
    width: '70%',
  },
  emailInput: {
    backgroundColor: 'white',
    borderWidth: 1,
    padding: 5,
    fontFamily: 'Oswald'
  },
  emailHelpText: {
    color: '#9b9b9b',
    fontSize: 12
  }
};