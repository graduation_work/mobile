import React, {useEffect, useState} from "react";
import {connect} from 'react-redux';
import {View, Image, TouchableOpacity, Dimensions} from 'react-native';
import {OswaldText} from "./StyledText";
import {ShopButton} from "./Buttons";
import {edit_cart, set_favourite, unset_favourite} from "../helpers";
import {addCartProduct, addFavourite, removeCartProduct, removeFavourite} from "../redux/actions";

const cardWidth = Dimensions.get('window').width / 2;

const ProductCard = props => {
  const [cart, setCart] = useState(false);
  const [favourite, setFavourite] = useState(false);

  const handleOnPress = () => props.navigation.navigate('Product', {
    title: props.product.name,
    id: props.product.id
  });

  useEffect(() => {
    if (cart !== (props.cart.indexOf(props.product.id) !== -1))
      setCart(!cart);
  }, [props.cart.length]);

  useEffect(() => {
    if (favourite !== (props.favourites.indexOf(props.product.id) !== -1))
      setFavourite(!favourite);
  }, [props.favourites.length]);

  const handleToCartClick = product => {
    if (props.cart.indexOf(product.id) !== -1) {
      props.removeFromCart(product.id);
      edit_cart(product, 0);
    } else {
      props.addToCart(product.id);
      edit_cart(product, 1);
    }
  };

  const handleToFavouritesClick = product => {
    if (props.favourites.indexOf(product.id) !== -1) {
      props.unsetFavourite(product.id);
      unset_favourite(product);
    } else {
      props.setFavourite(product.id);
      set_favourite(product);
    }
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={handleOnPress}>
        <Image
          source={{uri: props.product.image}}
          style={styles.image}
          resizeMode='contain'
        />
        <View style={styles.cardBody}>
          <OswaldText style={styles.priceText}>{props.product.price} Р</OswaldText>
          <OswaldText style={styles.nameText}>{props.product.name}</OswaldText>
          <View style={styles.buttonsBlock}>
            <ShopButton
              color={cart ? 'black' : 'white'}
              icon='ios-cart'
              style={styles.button}
              onPress={() => handleToCartClick(props.product)}
            />
            <ShopButton
              color={favourite ? 'black' : 'white'}
              icon='ios-heart'
              style={styles.button}
              onPress={() => handleToFavouritesClick(props.product)}
            />
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
}

export default connect(
  state => ({
    cart: state.cart,
    favourites: state.favourites,
  }),
  dispatch => ({
    setFavourite: id => dispatch(addFavourite(id)),
    unsetFavourite: id => dispatch(removeFavourite(id)),
    addToCart: id => dispatch(addCartProduct(id)),
    removeFromCart: id => dispatch(removeCartProduct(id))
  }))(ProductCard);

const styles = {
  container: {
    width: cardWidth,
    backgroundColor: 'white',
    margin: 1
  },
  image: {
    margin: 0,
    padding: 0,
    width: cardWidth,
    height: cardWidth
  },
  cardBody: {
    margin: 3
  },
  priceText: {
    fontSize: 18
  },
  nameText: {
    fontSize: 16
  },
  buttonsBlock: {
    marginTop: 3,
    flex: 1,
    flexDirection: 'row',
  },
  button: {
    borderRadius: 0.1,
    flex: 0.5,
    margin: 1,
  }
};