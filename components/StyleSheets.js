import {StyleSheet} from "react-native";

export const SignStyles = StyleSheet.create({
  container: {
    margin: 10,
    flex: 1,
    alignItems: 'center',
  },
  inputLabel: {
    fontFamily: 'Oswald',
    textTransform: 'uppercase'
  },
  header: {
    marginTop: 30,
    fontSize: 30,
    textTransform: 'uppercase',
  },
  body: {
    width: '80%',
    marginTop: 30,
  },
  buttonMain: {
    borderRadius: 0.1,
    backgroundColor: 'black',
    margin: 7,
  },
  buttonMainTitle: {
    fontFamily: 'Oswald',
    textTransform: 'uppercase',
  },
  buttonSecondary: {
    borderColor: 'black',
    borderRadius: 0.1,
    margin: 7,
  },
  buttonSecondaryTitle: {
    fontFamily: 'Oswald',
    textTransform: 'uppercase',
    color: 'black',
  }
});