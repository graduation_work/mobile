import React from 'react';
import {SignStyles} from "./StyleSheets";
import {SafeAreaView, ScrollView, View} from "react-native";
import {OswaldText} from "./StyledText";
import {Button, Input} from "react-native-elements";
import {Ionicons} from "@expo/vector-icons";

export default function SignUp(
  {
    isEntering,
    onChangeEmail,
    onChangePassword,
    onChangePasswordConfirmation,
    errorMessages,
    handleSignUpClick,
    navigation,
  }
) {
  return (
    <SafeAreaView>
      <ScrollView>
        <View style={SignStyles.container}>
          <OswaldText style={SignStyles.header}>Регистрация</OswaldText>
          <View style={SignStyles.body}>
            <Input
              leftIcon={<Ionicons
                name='ios-mail'
                size={24}
                color='#ccc'
              />}
              label='Электронная почта'
              labelStyle={SignStyles.inputLabel}
              onChangeText={value => onChangeEmail(value)}
              errorMessage={errorMessages.email}
            />
            <Input
              leftIcon={<Ionicons
                name='ios-lock'
                size={24}
                color='#ccc'
              />}
              label='Пароль'
              labelStyle={SignStyles.inputLabel}
              secureTextEntry={true}
              onChangeText={value => onChangePassword(value)}
            />
            <Input
              leftIcon={<Ionicons
                name='ios-lock'
                size={24}
                color='#ccc'
              />}
              label='Подтвердите Пароль'
              labelStyle={SignStyles.inputLabel}
              secureTextEntry={true}
              onChangeText={value => onChangePasswordConfirmation(value)}
              errorMessage={errorMessages.password}
            />
            <Button
              type='solid'
              title={isEntering ? 'Пожалуйста, подождите' : 'Зарегистрироваться'}
              buttonStyle={SignStyles.buttonMain}
              titleStyle={SignStyles.buttonMainTitle}
              onPress={handleSignUpClick}
            />
            <Button
              type='outline'
              title='Есть аккаунт? Вход'
              buttonStyle={SignStyles.buttonSecondary}
              titleStyle={SignStyles.buttonSecondaryTitle}
              onPress={() => navigation.navigate('SignIn')}
            />
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}