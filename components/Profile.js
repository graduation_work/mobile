import React from 'react';
import {SafeAreaView, View, StyleSheet, FlatList, TouchableOpacity, ScrollView} from "react-native";
import {OswaldText} from "./StyledText";
import {ShopButton} from "./Buttons";

export default function Profile({orders, email, handleExitClick, loadOrders, handleOrderClick}) {
  return (
    <SafeAreaView>
      <View style={styles.container}>
        <OswaldText style={styles.email}>EMAIL: {email}</OswaldText>
        <ShopButton
          title="Выход"
          onPress={handleExitClick}
        />

        <OswaldText style={styles.ordersHeader}>Мои заказы</OswaldText>
        <FlatList
          style={styles.list}
          onEndReached={loadOrders}
          data={orders}
          renderItem={({item}) => {
            const date = (new Date(item.created_at)).toLocaleDateString('ru-RU');

            return (
              <TouchableOpacity
                style={styles.card}
                onPress={() => handleOrderClick({
                  id: item.id,
                  content: JSON.parse(item.content),
                  date: date,
                  price: item.price
                })}
              >
                <OswaldText style={styles.ordersText}>Заказ #{item.id}</OswaldText>
                <OswaldText style={styles.ordersText}>Сумма заказа {item.price} Р</OswaldText>
                <OswaldText style={styles.ordersText}>Дата оформления {date}</OswaldText>
              </TouchableOpacity>
            );
          }}
        />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    margin: 10
  },
  email: {
    fontSize: 24,
    marginBottom: 10,
  },
  ordersHeader: {
    fontSize: 24,
    marginTop: 10,
    marginBottom: 10,
    textTransform: 'uppercase',
  },
  ordersText: {
    fontSize: 18,
  },
  list: {
    height: '75%',
  },
  card: {
    backgroundColor: 'white',
    padding: 10,
    marginBottom: 10,
  }
});