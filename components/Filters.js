import React from 'react';
import {ScrollView, StyleSheet, TextInput, View} from "react-native";
import {OswaldText} from "./StyledText";
import {Picker} from "@react-native-community/picker";
import {ShopButton} from "./Buttons";

export default function Filters(
  {
    searchName, setSearchName,
    priceFrom, setPriceFrom,
    priceTo, setPriceTo,
    categoryId, setCategoryId,
    categories,
    handleApplyClick,
    handleResetClick,
  }
) {
  return (<ScrollView>
    <View style={styles.container}>
      <View>
        <OswaldText style={styles.headerText}>Название товара</OswaldText>
        <TextInput
          style={styles.textInput}
          placeholder='Введите название товара или его часть'
          onChange={e => setSearchName(e.nativeEvent.text)}
          defaultValue={searchName}
        />
      </View>
      <View>
        <OswaldText style={styles.headerText}>Цена</OswaldText>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <TextInput
            style={[styles.textInput, {flex: 0.5, marginRight: 10}]}
            placeholder='от'
            onChange={e => setPriceFrom(e.nativeEvent.text)}
            defaultValue={priceFrom}
          />
          <TextInput
            style={[styles.textInput, {flex: 0.5, marginLeft: 10}]}
            placeholder='до'
            onChange={e => setPriceTo(e.nativeEvent.text)}
            defaultValue={priceTo}
          />
        </View>
      </View>
      <View>
        <OswaldText style={styles.headerText}>Категория</OswaldText>
        <View style={styles.pickerWrapper}>
          <Picker
            selectedValue={categoryId}
            style={styles.picker}
            onValueChange={(value) => setCategoryId(value)}
          >
            <Picker.Item label='Выберите категорию' value={null} color='#ccc'/>
            {
              categories.map(category => <Picker.Item
                key={`category_${category.id}`}
                label={category.name}
                value={category.id}
              />)
            }
          </Picker>
        </View>
      </View>
      <ShopButton onPress={handleApplyClick} color='black' title={'Применить'}/>
      <ShopButton onPress={handleResetClick} color='white' title={'Сбросить'} style={{marginTop: 10}}/>
    </View>
  </ScrollView>);
}


const styles = StyleSheet.create({
  container: {
    margin: 10,
  },
  headerText: {
    marginBottom: 10,
    fontSize: 20,
    textTransform: 'uppercase',
  },
  textInput: {
    borderColor: '#cccccc',
    borderWidth: 1,
    backgroundColor: 'white',
    fontSize: 20,
    fontFamily: 'Oswald',
    padding: 10,
    marginBottom: 10,
  },
  pickerWrapper: {
    backgroundColor: '#ccc',
    padding: 1,
    marginBottom: 10,
  },
  picker: {
    backgroundColor: 'white',
  },
});