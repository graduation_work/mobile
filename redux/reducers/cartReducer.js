import {
  ADD_CART_PRODUCT,
  REMOVE_CART_PRODUCT,
  SET_CART
} from "../actions/cartActions";

const cart = (state = [], action) => {
  switch (action.type) {
    case SET_CART:
      return action.data;
    case ADD_CART_PRODUCT:
      return [
        ...state,
        action.id
      ];
    case REMOVE_CART_PRODUCT:
      return state.filter(id => id !== action.id);
    default:
      return state;
  }
};

export default cart;