import {
  SET_FAVOURITES,
  ADD_FAVOURITE_PRODUCT,
  REMOVE_FAVOURITE_PRODUCT
} from "../actions/favouritesActions";

const favourites = (state = [], action) => {
  switch (action.type) {
    case SET_FAVOURITES:
      return action.data;
    case ADD_FAVOURITE_PRODUCT:
      return [
        ...state,
        action.id
      ];
    case REMOVE_FAVOURITE_PRODUCT:
      return state.filter(id => id !== action.id);
    default:
      return state;
  }
};

export default favourites;