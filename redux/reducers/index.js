import cart from "./cartReducer";
import categories from "./categoriesReducer";
import favourites from "./favouritesReducer";
import orders from "./ordersReducer";
import profile from "./profileReducer";
import filters from "./filtersReducer";

export {
  cart,
  categories,
  favourites,
  orders,
  profile,
  filters,
}