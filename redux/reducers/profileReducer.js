import {
  SIGNIN,
} from "../actions/profileActions";

const profile = (state = {authorized: false}, action) => {
  switch (action.type) {
    case SIGNIN:
      return {
        authorized: true,
        ...action.profile
      };
    default:
      return state;
  }
};

export default profile;