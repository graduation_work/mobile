import {SET_FILTERS} from "../actions/filtersActions";

const initialState = {
  name: null,
  price_from: null,
  price_to: null,
  category_id: null,
};

const filters = (state = initialState, action) => {
  switch (action.type) {
    case SET_FILTERS:
      return action.filters;
    default:
      return state;
  }
};

export default filters;