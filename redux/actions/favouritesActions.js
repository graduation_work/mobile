export const SET_FAVOURITES = 'SET_FAVOURITES';
export const ADD_FAVOURITE_PRODUCT = 'ADD_FAVOURITE_PRODUCT';
export const REMOVE_FAVOURITE_PRODUCT = 'REMOVE_FAVOURITE_PRODUCT';


export const setFavourites = data => ({
  type: SET_FAVOURITES,
  data: data
});

export const addFavourite = id => ({
  type: ADD_FAVOURITE_PRODUCT,
  id: id
});

export const removeFavourite = id => ({
  type: REMOVE_FAVOURITE_PRODUCT,
  id: id
});