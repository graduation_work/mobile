export const SET_FILTERS = 'SET_FILTERS';

export const setFilters = (searchName, priceFrom, priceTo, categoryId) => ({
  type: SET_FILTERS,
  filters: {
    name: searchName,
    price_from: priceFrom,
    price_to: priceTo,
    category_id: categoryId,
  }
});