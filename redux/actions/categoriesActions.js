export const SET_CATEGORIES = 'SET_CATEGORIES';

export const setCategories = (data) => ({
  type: SET_CATEGORIES,
  categories: data
})