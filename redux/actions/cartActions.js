export const SET_CART = 'SET_CART';
export const ADD_CART_PRODUCT = 'ADD_CART_PRODUCT';
export const REMOVE_CART_PRODUCT = 'REMOVE_CART_PRODUCT';

export const setCart = data => ({
  type: SET_CART,
  data: data
});

export const addCartProduct = id => ({
  type: ADD_CART_PRODUCT,
  id: id
});

export const removeCartProduct = id => ({
  type: REMOVE_CART_PRODUCT,
  id: id
})