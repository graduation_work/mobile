import React from 'react';
import * as reducers from './redux/reducers';
import thunk from 'redux-thunk';
import {Provider} from 'react-redux';
import {combineReducers, applyMiddleware, createStore} from 'redux';

import useCachedResources from './hooks/useCachedResources';

import Main from "./Main";

const store = createStore(combineReducers(reducers), applyMiddleware(thunk));

export default function App() {
  const isLoadingComplete = useCachedResources();

  if (!isLoadingComplete) {
    return null;
  }

  return (
    <Provider store={store}>
      <Main/>
    </Provider>
  );
}
