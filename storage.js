import {AsyncStorage} from "react-native";

export const storeData = async (key, value) => {
  try {
    await AsyncStorage.setItem(`@${key}`, JSON.stringify(value));
  } catch (e) {
    // saving error
  }
};

export const getData = async key => {
  try {
    const value = await AsyncStorage.getItem(`@${key}`);
    return value != null ? JSON.parse(value) : null;
  } catch (e) {
    // error reading value
  }
};

export const deleteData = async key => {
  try {
    await AsyncStorage.removeItem(`@${key}`);
  } catch (e) {
    // remove error
  }
}