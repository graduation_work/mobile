import * as React from 'react';
import {connect} from "react-redux";
import {useFocusEffect} from "@react-navigation/native";
import {useCallback, useState} from "react";
import * as api from "../api";
import {getData} from "../storage";
import {FAVOURITE_PRODUCTS_KEY} from "../helpers";
import Cards from "../components/Cards";

const FavouritesScreen = props => {
  const [products, setProducts] = useState([]);
  const [page, setPage] = useState(1);
  const [lastPage, setLastPage] = useState(1);

  const getDataFromStorage = () => getData(FAVOURITE_PRODUCTS_KEY).then(result => {
    if (result !== null) {
      setProducts(result.filter(({id}) => props.favourites.indexOf(id) !== -1));
    }
  });

  useFocusEffect(useCallback(() => {
    if (props.auth) {
      api.getFavourites(page).then(response => {
        setProducts(response.data.products.filter(({id}) => props.favourites.indexOf(id) !== -1));
        setLastPage(response.data.last_page)
      }).catch(() => getDataFromStorage());
    } else {
      getDataFromStorage()
    }
  }, [props.favourites.length]));

  const loadProducts = () => {
    if (props.auth && page < lastPage) {
      api.getFavourites(page + 1).then(response => setProducts([
        ...products,
        ...response.data.products
      ]));
      setPage(page + 1);
    }
  }

  return <Cards
    products={products}
    loadProducts={loadProducts}
    navigation={props.navigation}
  />;
}

export default connect(state => ({
  auth: state.profile.authorized,
  favourites: state.favourites,
}))(FavouritesScreen);