import React from 'react';
import {ScrollView, View, StyleSheet} from "react-native";
import {OswaldText} from "../components/StyledText";

export default function OrderScreen(props) {
  const order = props.route.params;

  return <ScrollView>
    <View style={styles.container}>
      <OswaldText style={styles.text}>Сумма заказа {order.price} Р</OswaldText>
      <OswaldText style={styles.text}>Дата оформления {order.date}</OswaldText>
      {
        order.content.map(item =>
          <View key={`order_item_${item.id}`} style={styles.card}>
            <OswaldText style={styles.text}>Название: {item.name}</OswaldText>
            <OswaldText style={styles.text}>Описание: {item.description}</OswaldText>
            <OswaldText style={styles.text}>Цена {item.price} Р</OswaldText>
            <OswaldText style={styles.text}>Количество {item.count} шт.</OswaldText>
          </View>
        )
      }
    </View>
  </ScrollView>
}

const styles = StyleSheet.create({
  container: {
    margin: 10,
  },
  card: {
    backgroundColor: 'white',
    padding: 10,
    marginTop: 10,
  },
  text: {
    fontSize: 18
  }
})