import React, {useState} from 'react';
import * as api from '../api'
import {getData, storeData} from "../storage";
import {CART_PRODUCTS_KEY, FAVOURITES_KEY} from "../helpers";
import * as Updates from "expo-updates";
import SignUp from "../components/SignUp";

export default function SignUpScreen({navigation}) {
  const [email, onChangeEmail] = React.useState('');
  const [password, onChangePassword] = React.useState('');
  const [passwordConfirmation, onChangePasswordConfirmation] = React.useState('');
  const [errors, setErrors] = useState({});
  const [isEntering, setEntering] = useState(false);

  const setCart = (next) => {
    getData(CART_PRODUCTS_KEY).then(cartResult => {
      if (cartResult) {
        api.editCart(cartResult.map(item => ({
          product_id: item.id,
          count: item.count,
        })))
          .then(() => next())
          .catch(error => console.log('signup setcart:', error.response.data));
      } else {
        next();
      }
    });
  };

  const setFavourite = () => {
    getData(FAVOURITES_KEY).then(favouritesResult => {
      if (favouritesResult) {
        api.setFavourite(favouritesResult)
          .then(() => Updates.reloadAsync())
          .catch(error => console.log('signup setfav:', error.response.data));
      } else {
        Updates.reloadAsync();
      }
    });
  };

  const handleSignUpClick = () => {
    if (email && password && passwordConfirmation)
      api.signUp(email.toLowerCase(), password, passwordConfirmation)
        .then(response => {
          setEntering(true);
          storeData('token', response.data.token).then(
            () => {
              api.setToken(response.data.token);
              setCart(setFavourite);
            }
          );
        })
        .catch(error => {
          if (error.response && error.response.data.errors) {
            setErrors(error.response.data.errors);
          }
        })
  }

  const errorMessages = (() => {
    const messages = {};

    if (errors.email && errors.email[0])
      messages['email'] = errors.email[0];
    else
      messages['email'] = null;

    if (errors.password && errors.password[0])
      messages['password'] = errors.password[0];
    else if (errors.passwordConfirmation && errors.passwordConfirmation[0])
      messages['password'] = errors.passwordConfirmation[0];
    else
      messages['password'] = null;

    return messages;
  })();

  return <SignUp
    isEntering={isEntering}
    onChangePasswordConfirmation={onChangePasswordConfirmation}
    onChangePassword={onChangePassword}
    onChangeEmail={onChangeEmail}
    errorMessages={errorMessages}
    handleSignUpClick={handleSignUpClick}
    navigation={navigation}
  />
}