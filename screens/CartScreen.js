import React, {useState, useCallback, useEffect} from 'react';
import {connect} from 'react-redux';
import * as api from '../api';
import {CART_PRODUCTS_KEY, CART_KEY} from "../helpers";
import {removeCartProduct, setCart} from "../redux/actions";
import {edit_cart} from "../helpers";
import {useFocusEffect} from '@react-navigation/native';
import {getData, storeData} from "../storage";
import Cart from "../components/Cart";
import {Vibration, Platform, Alert} from 'react-native';
import {Notifications} from 'expo';
import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';

const CartScreen = props => {
  const [products, setProducts] = useState([]);
  const [token, setToken] = useState(null);
  const [notification, setNotification] = useState({});
  const [email, onChangeEmail] = useState(null);
  const [orderInProcess, setOrderInProcess] = useState(false);
  const [emailError, setEmailError] = useState(false);

  const registerForPushNotificationsAsync = async () => {
    if (Constants.isDevice) {
      const {status: existingStatus} = await Permissions.getAsync(Permissions.NOTIFICATIONS);
      let finalStatus = existingStatus;
      if (existingStatus !== 'granted') {
        const {status} = await Permissions.askAsync(Permissions.NOTIFICATIONS);
        finalStatus = status;
      }
      if (finalStatus !== 'granted') {
        alert('Failed to get push token for push notification!');
        return;
      }
      const token = await Notifications.getExpoPushTokenAsync();
      setToken(token);
    } else {
      alert('Must use physical device for Push Notifications');
    }

    if (Platform.OS === 'android') {
      await Notifications.createChannelAndroidAsync('default', {
        name: 'default',
        sound: true,
        priority: 'max',
        vibrate: [0, 250, 250, 250],
        badge: false
      });
    }
  };

  const handleNotification = notification => {
    Vibration.vibrate();
    setNotification(notification);
  };

  useEffect(() => {
    registerForPushNotificationsAsync();
    Notifications.addListener(handleNotification);
  }, []);

  useFocusEffect(useCallback(() => {
    if (props.auth) {
      api.getCart().then(response => {
        const newStateProducts = response.data.filter(({id}) => props.cart.indexOf(id) !== -1);
        setProducts(newStateProducts);
      });
    } else {
      getData(CART_PRODUCTS_KEY).then(result => {
        if (result !== null) {
          const newStateProducts = result.filter(({id}) => props.cart.indexOf(id) !== -1);
          setProducts(newStateProducts);
        }
      });
    }
  }, [props.cart.length]));

  const handleCountChange = (product, count) => {
    edit_cart(product, count);

    if (count === 0) {
      props.removeFromCart(product.id);
    } else {
      setProducts(products.map(item => {
        if (item.id === product.id)
          item.count = count;

        return item;
      }));
    }
  };

  const handleItemClick = product => props.navigation.navigate('Product', {
    title: product.name,
    id: product.id,
  });

  const handleBuyClick = () => {
    if (products.length === 0) {
      Alert.alert('Предупреждение', 'Корзина пуста');
      return;
    }

    if (props.auth) {
      setOrderInProcess(true);
      return api.createOrder().then(response => {
        setProducts([]);
        props.flushCart();
        sendPushNotification(response.data.id, response.data.price, props.email)
        setOrderInProcess(false);
      });
    }

    if (!email) {
      Alert.alert('Предупреждение', 'Введите свою электронную почту или войдите в аккаунт');
      return setEmailError(true);
    }

    setEmailError(false);
    setOrderInProcess(true);

    const order = products.map(product => ({
      product_id: product.id,
      count: product.count
    }));

    return api.createOrder(order, email)
      .then(response => {
        props.flushCart();
        setProducts([]);
        sendPushNotification(response.data.id, response.data.price, email.toLowerCase());
        storeData(CART_PRODUCTS_KEY, []);
        storeData(CART_KEY, []);
        setOrderInProcess(false);
      })
      .catch(error => {
        if (error.response && error.response.data && error.response.data.errors && error.response.data.errors.email) {
          Alert.alert('Введен неверный email', error.response.data.errors.email[0]);
          setEmailError(true);
        }
      });
  }

  const sendPushNotification = async (orderId, price, email) => {
    const message = `Заказ №${orderId} стоимостью ${price} Р оформлен! Подробности отправлены на почту ${email}`;

    Alert.alert('Заказ оформлен', message);

    await fetch('https://exp.host/--/api/v2/push/send', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Accept-encoding': 'gzip, deflate',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        to: token,
        sound: 'default',
        title: 'Оформление заказа',
        body: message,
        _displayInForeground: true,
        image: require('../assets/images/splash.png')
      }),
    });
  };

  return <Cart
    auth={props.auth}
    products={products}
    onChangeEmail={onChangeEmail}
    emailError={emailError}
    orderInProcess={orderInProcess}
    handleCountChange={handleCountChange}
    handleBuyClick={handleBuyClick}
    handleItemClick={handleItemClick}
    totalPrice={products.reduce((acc, item) => {
      acc += item.price * item.count;
      return acc;
    }, 0)}
  />;
}

export default connect(
  state => ({
    auth: state.profile.authorized,
    email: state.profile.email,
    cart: state.cart,
  }),
  dispatch => ({
    removeFromCart: id => dispatch(removeCartProduct(id)),
    flushCart: () => dispatch(setCart([])),
  })
)(CartScreen);