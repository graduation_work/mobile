import React, {useCallback, useState} from 'react';
import {connect} from "react-redux";
import * as Updates from 'expo-updates';
import Profile from "../components/Profile";
import {AsyncStorage} from "react-native";
import {useFocusEffect} from '@react-navigation/native';
import * as api from "../api";

const ProfileScreen = ({profile, navigation}) => {
  const [orders, setOrders] = useState([]);
  const [page, setPage] = useState(1);
  const [lastPage, setLastPage] = useState(1);

  useFocusEffect(useCallback(() => {
    api.getOrders(1).then(response => {
      setOrders(response.data.data);
      setPage(response.data.current_page);
      setLastPage(response.data.last_page);
    });
  }, []));

  const loadOrders = () => {
    if (page < lastPage) {
      api.getOrders(page + 1).then(response => setOrders([
        ...orders,
        ...response.data.data
      ]));
      setPage(page + 1);
    }
  }

  const handleExitClick = () => AsyncStorage.clear().then(() => Updates.reloadAsync());

  const handleOrderClick = data => navigation.navigate('Order', data);

  return <Profile
    orders={orders}
    email={profile.email}
    handleExitClick={handleExitClick}
    loadOrders={loadOrders}
    handleOrderClick={handleOrderClick}
  />
}

export default connect(
  state => ({
    profile: state.profile
  })
)(ProfileScreen);