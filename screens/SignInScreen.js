import React, {useState} from 'react';
import * as api from "../api";
import {getData, storeData} from "../storage";
import {CART_PRODUCTS_KEY, FAVOURITES_KEY} from "../helpers";
import * as Updates from 'expo-updates';
import SignIn from "../components/SignIn";

export default function SignInScreen({navigation}) {
  const [email, onChangeEmail] = useState('');
  const [password, onChangePassword] = useState('');
  const [invalidCredentials, setInvalidCredentials] = useState(false);
  const [isEntering, setEntering] = useState(false);

  const setCart = (next) => {
    getData(CART_PRODUCTS_KEY).then(cartResult => {
      if (cartResult && cartResult.length > 0) {
        api.editCart(cartResult.map(item => ({
          product_id: item.id,
          count: item.count,
        })))
          .then(() => next())
          .catch(error => console.log('signin setcart:', error.response.data));
      } else {
        next();
      }
    });
  };

  const setFavourite = () => {
    getData(FAVOURITES_KEY).then(favouritesResult => {
      if (favouritesResult && favouritesResult.length > 0) {
        api.setFavourite(favouritesResult)
          .then(() => Updates.reloadAsync())
          .catch(error => console.log('signin setfav:', error.response.data));
      } else {
        Updates.reloadAsync();
      }
    });
  };

  const handleSignInClick = () => {
    if (email && password) {
      api.signIn(email.toLowerCase(), password)
        .then(response => {
          setEntering(true);
          storeData('token', response.data.token).then(
            () => {
              api.setToken(response.data.token);
              setCart(setFavourite);
            }
          );
        })
        .catch(() => setInvalidCredentials(true));
    } else {
      setInvalidCredentials(true);
    }
  };

  return <SignIn
    isEntering={isEntering}
    handleSignInClick={handleSignInClick}
    onChangeEmail={onChangeEmail}
    onChangePassword={onChangePassword}
    invalidCredentials={invalidCredentials}
    navigation={navigation}
  />
}