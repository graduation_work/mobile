import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import * as api from '../api';
import Cards from "../components/Cards";

const ShopScreen = props => {
  const [products, setProducts] = useState([]);
  const [page, setPage] = useState(1);
  const [lastPage, setLastPage] = useState(1);

  useEffect(() => {
    const filters = props.filters;
    filters.page = 1;
    setPage(1);
    api.getProducts(filters).then(response => {
      setProducts(response.data.products);
      setLastPage(response.data.last_page)
    });
  }, [
    props.filters.name,
    props.filters.price_from,
    props.filters.price_to,
    props.filters.category_id,
  ]);

  const loadProducts = () => {
    if (Object.values(props.filters).length >= 4) {
      const filters = props.filters;
      filters.page = page + 1;
      if (page < lastPage) {
        api.getProducts(filters).then(response => setProducts([
          ...products,
          ...response.data.products
        ]));
        setPage(page + 1);
      }
    }
  };

  return <Cards
    products={products}
    loadProducts={loadProducts}
    navigation={props.navigation}
  />;
}

export default connect(state => ({
  filters: state.filters,
}))(ShopScreen);