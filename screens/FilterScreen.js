import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import * as api from '../api';
import {setFilters} from "../redux/actions";
import {setCategories} from "../redux/actions/categoriesActions";
import Filters from "../components/Filters";
import {AppLoading} from "expo";

const FilterScreen = props => {
  const [searchName, setSearchName] = useState(null);
  const [priceFrom, setPriceFrom] = useState(null);
  const [priceTo, setPriceTo] = useState(null);
  const [categoryId, setCategoryId] = useState(null);
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    if (Object.values(props.filters).length >= 4) {
      setSearchName(props.filters.name);
      setPriceFrom(props.filters.price_from);
      setPriceTo(props.filters.price_to);
      setCategoryId(props.filters.category_id);
      setLoading(false);
    }
  }, [
    props.filters.name,
    props.filters.price_from,
    props.filters.price_to,
    props.filters.category_id,
  ]);

  useEffect(() => {
    if (props.categories.length === 0)
      api.getCategories().then(response => props.setCategories(response.data));
  }, [props.categories.length]);

  const handleApplyClick = () => {
    props.setFilters(searchName, priceFrom, priceTo, categoryId);
    props.navigation.goBack();
  };

  const handleResetClick = () => {
    props.setFilters(null, null, null, null);
    setSearchName(null);
    setPriceFrom(null);
    setPriceTo(null);
    setCategoryId(null);
  };

  if (isLoading) return <AppLoading/>;

  return <Filters
    searchName={searchName}
    setSearchName={setSearchName}
    priceFrom={priceFrom}
    setPriceFrom={setPriceFrom}
    priceTo={priceTo}
    setPriceTo={setPriceTo}
    categoryId={categoryId}
    setCategoryId={setCategoryId}
    categories={props.categories}
    handleApplyClick={handleApplyClick}
    handleResetClick={handleResetClick}
  />
}

export default connect(
  state => ({
    filters: state.filters,
    categories: state.categories,
  }),
  dispatch => ({
    setFilters: (searchName, priceFrom, priceTo, categoryId) => dispatch(setFilters(searchName, priceFrom, priceTo, categoryId)),
    setCategories: categories => dispatch(setCategories(categories)),
  })
)(FilterScreen);