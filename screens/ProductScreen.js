import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import * as api from '../api';
import {addFavourite, removeFavourite, addCartProduct, removeCartProduct} from "../redux/actions";
import {edit_cart} from "../helpers";
import {set_favourite, unset_favourite} from "../helpers";
import Product from "../components/Product";

const ProductScreen = props => {
  const [product, setProduct] = useState({});
  const [previewIndex, setPreviewIndex] = useState(0);
  const [cart, setCart] = useState(false);
  const [favourite, setFavourite] = useState(false);

  useEffect(() => {
    api.showProduct(props.route.params.id).then(response => setProduct(response.data));
  }, []);

  useEffect(() => {
    if (cart !== (props.cart.indexOf(product.id) !== -1))
      setCart(!cart);
  }, [props.cart.length, product.id]);

  useEffect(() => {
    if (favourite !== (props.favourites.indexOf(product.id) !== -1))
      setFavourite(!favourite);
  }, [props.favourites.length, product.id]);

  if (!product.images) return null;

  const handleToCartClick = product => {
    if (props.cart.indexOf(product.id) !== -1) {
      props.removeFromCart(product.id);
      edit_cart(product, 0);
    } else {
      props.addToCart(product.id);
      edit_cart(product, 1);
    }
  };

  const handleToFavouritesClick = product => {
    if (props.favourites.indexOf(product.id) !== -1) {
      props.unsetFavourite(product.id);
      unset_favourite(product);
    } else {
      props.setFavourite(product.id);
      set_favourite(product);
    }
  };

  return <Product
    product={product}
    previewIndex={previewIndex}
    setPreviewIndex={setPreviewIndex}
    handleToCartClick={handleToCartClick}
    handleToFavouritesClick={handleToFavouritesClick}
    cart={cart}
    favourite={favourite}
  />
}

export default connect(
  state => ({
    favourites: state.favourites,
    cart: state.cart,
  }),
  dispatch => ({
    setFavourite: id => dispatch(addFavourite(id)),
    unsetFavourite: id => dispatch(removeFavourite(id)),
    addToCart: id => dispatch(addCartProduct(id)),
    removeFromCart: id => dispatch(removeCartProduct(id))
  })
)(ProductScreen);

