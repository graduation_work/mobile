import {getData, storeData} from "./storage";
import * as api from './api';

export const FAVOURITES_KEY = 'favourites';
export const FAVOURITE_PRODUCTS_KEY = 'favourite_products';
export const CART_KEY = 'cart';
export const CART_PRODUCTS_KEY = 'cart_products';

export const set_favourite = product => {
  getData('token').then(token => {
    if (token !== null) {
      api.setFavourite([product.id]);
    }

    if (product.images)
      product.image = product.images[0] ? product.images[0].image : null;

    getData(FAVOURITES_KEY).then(result => {
      storeData(FAVOURITES_KEY, (() => {
        if (!result)
          return [product.id];

        const favourites = new Set(result);
        favourites.add(product.id);
        return Array.from(favourites);
      })());
    });

    getData(FAVOURITE_PRODUCTS_KEY).then(result => {
      storeData(FAVOURITE_PRODUCTS_KEY, (() => {
        if (!result)
          return [product];

        const favouriteProducts = new Set(result);
        favouriteProducts.add(product);
        return Array.from(favouriteProducts);
      })());
    });
  })
}

export const unset_favourite = product => {
  getData('token').then(token => {
    if (token !== null) {
      api.unsetFavourite([product.id]);
    }

    getData(FAVOURITES_KEY).then(result => {
      storeData(FAVOURITES_KEY, (() => {
        if (!result)
          return [];

        const favourites = new Set(result);
        favourites.delete(product.id);
        return Array.from(favourites);
      })());
    });

    getData(FAVOURITE_PRODUCTS_KEY).then(result => {
      storeData(FAVOURITE_PRODUCTS_KEY, (() => {
        if (!result)
          return [];

        return result.filter(item => item.id !== product.id)
      })());
    });
  })
}

export const edit_cart = (product, count) => {
  getData('token').then(token => {
    if (token !== null) {
      api.editCart([{
        product_id: product.id,
        count: count,
      }]);
    }

    if (product.images) product.image = product.images[0] ? product.images[0] : null;

    const data = {...product, count: count};

    getData(CART_KEY).then(result => {
      storeData(CART_KEY, (() => {
        if (!result)
          return count === 0 ? [] : [product.id];

        const cart = new Set(result);

        if (count === 0) {
          cart.delete(product.id);
        } else {
          cart.add(product.id);
        }

        return Array.from(cart);
      })())
    });

    getData(CART_PRODUCTS_KEY).then(result => {
      storeData(CART_PRODUCTS_KEY, (() => {
        if (!result) return count == 0 ? [] : [data];

        if (!result.find(item => item.id === product.id))
          result.push(data);

        return count == 0
          ? result.filter(item => item.id !== product.id)
          : result.map(item => {
            if (item.id === product.id)
              item.count = count;

            return item;
          });
      })());
    });
  })
}

